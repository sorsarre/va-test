#include <iostream>
#include <thread>
#include <boost/program_options.hpp>

#include "buffer_pool_impl.h"
#include "proc.h"
#include "hash_collector_impl.h"
#include "hasher.h"
#include "block_reader.h"

struct args
{
    std::string input;
    std::string output;
    uint64_t block_size;
    uint32_t threads;
    uint32_t flush_threshold;
    bool invoke_help;
};

args process_args(int argc, char** argv) {
    namespace po = boost::program_options;
    po::options_description desc("Allowed options");
    desc.add_options()
            ("help,h", "produce help message")
            ("input,i", po::value<std::string>()->default_value("-"), "input file (- for stdin, default)")
            ("output,o", po::value<std::string>()->default_value("-"), "output file(- for stdout, default)")
            ("block,b", po::value<uint64_t>()->default_value(1024*1024), "block size")
            ("threads,t", po::value<uint32_t>()->default_value(0), "number of threads to use")
            ("flush,f", po::value<uint32_t>()->default_value(128), "number of blocks to process before flushing the results");

    po::positional_options_description pd;
    pd.add("input", 1);
    pd.add("output", 2);

    po::variables_map vm;
    po::store(
            po::command_line_parser(argc, argv)
                    .options(desc)
                    .positional(pd)
                    .run(), vm);
    po::notify(vm);

    args ret;
    if (vm.count("help")) {
        ret.invoke_help = true;
        std::cout << "va_test [OPTIONS] [INPUT] [OUTPUT] [BLOCKSIZE]" << std::endl;
        std::cout << desc << std::endl;
        return ret;
    }

    ret.invoke_help = false;
    ret.input = vm["input"].as<std::string>();
    ret.output = vm["output"].as<std::string>();
    ret.block_size = vm["block"].as<uint64_t>();
    ret.threads = vm["threads"].as<uint32_t>();
    ret.flush_threshold = vm["flush"].as<uint32_t>();
}

int main(int argc, char** argv) {
    try {
        auto args = process_args(argc, argv);
        if (args.invoke_help) {
            return 0;
        }

        size_t threads = args.threads ?
                         args.threads :
                         std::thread::hardware_concurrency();

        size_t buffers = 4 * std::min<size_t>(threads, 1);

        auto reader = hasher::block_reader::open(
                hasher::file_handle::open(
                        args.input,
                        hasher::file_handle::direction::input));

        auto collector = std::make_shared<hasher::hash_collector_impl>(
                hasher::file_handle::open(
                        args.output,
                        hasher::file_handle::direction::output));

        auto pool = std::make_shared<hasher::buffer_pool_impl>(
                args.block_size,
                buffers);

        auto proc = hasher::proc::create(threads);
        auto hash = std::make_shared<hasher::hasher>(
                pool,
                proc,
                collector,
                args.flush_threshold);

        // Main loop -- read buffers, feed them all
        while (!reader->done()) {
            auto buffer = pool->acquire();
            reader->read(buffer);
            hash->push(buffer);
        }

        hash->done();
    } catch(const std::exception& e) {
        std::cerr << "Error: " << e.what() << std::endl;
        return 1;
    }

    return 0;
}