#include <stdexcept>

#include "file_handle.h"

//----------------------------------------------------------------------
hasher::file_handle::ptr hasher::file_handle::open_stdin() {
    return ptr(new file_handle(stdin, true));
}

//----------------------------------------------------------------------
hasher::file_handle::ptr hasher::file_handle::open_stdout() {
    return ptr(new file_handle(stdout, true));
}

//----------------------------------------------------------------------
hasher::file_handle::ptr hasher::file_handle::open(const std::string& name, hasher::file_handle::direction dir) {
    if (name == "-") {
        switch(dir) {
            case direction::input:
                return open_stdin();
            case direction::output:
                return open_stdout();
        }
    }

    const char* d = (dir == direction::input) ? "rb" : "wb";
    auto file = fopen(name.c_str(), d);
    if (!file) {
        throw std::runtime_error("Could not open file: " + name);
    }

    return ptr(new file_handle(file, false));
}

//----------------------------------------------------------------------
hasher::file_handle::file_handle(file_handle&& fh) {
    _file = fh._file;
    _external = fh._external;
    fh._file = nullptr;
    fh._external = true;
}

//----------------------------------------------------------------------
FILE* hasher::file_handle::get() {
    return _file;
}

//----------------------------------------------------------------------
hasher::file_handle::~file_handle() {
    if (!_external) {
        fclose(_file);
    }
}

//----------------------------------------------------------------------
hasher::file_handle::file_handle(FILE* handle, bool external)
    : _file(handle)
    , _external(external)
{
}
