#pragma once
#include <string>
#include <memory>
#include <cstdio>

namespace hasher
{
    //----------------------------------------------------------------------
    class file_handle
    {
    public:
        enum class direction {
            input, output
        };

        typedef std::shared_ptr<file_handle> ptr;

        static ptr open_stdin();
        static ptr open_stdout();
        static ptr open(const std::string& name, direction dir);

        file_handle(file_handle&& fh);

        FILE* get();

        ~file_handle();
    private:
        file_handle(FILE* handle, bool external);

        FILE* _file;
        bool _external;
    };
}