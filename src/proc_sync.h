#pragma once

#include "proc.h"

namespace hasher
{
    class proc_sync: public proc
    {
    public:
        void stop() override;
        void push(std::shared_ptr<command> cmd) override;
    };
}