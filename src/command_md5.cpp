#include "command_md5.h"
#include "md5/md5.h"

using namespace hasher;

command_md5::command_md5(
        uint64_t index,
        shared_buffer buffer,
        std::shared_ptr<hash_collector> collector,
        std::shared_ptr<buffer_pool> pool)
        : _index(index)
        , _buffer(buffer)
        , _collector(collector)
        , _pool(pool)
{

}

namespace {
    static const char* __hex = "0123456789abcdef";
}

void command_md5::exec() const {
    // 1. calculate hash
    // 2. send over the result
    // 3. release buffer back into the pool
    MD5_CTX ctx;
    uint8_t hash[16];
    char result[33];
    result[32] = '\0';

    MD5_Init(&ctx);
    MD5_Update(&ctx, _buffer.get(), _buffer.size());
    MD5_Final(&hash[0], &ctx);

    for (int iter = 0; iter < 16; ++iter)
    {
        result[iter*2] = __hex[((hash[iter] >> 4) & 0x0F)];
        result[iter*2+1] = __hex[(hash[iter] & 0x0F)];
    }

    _collector->collect(_index, result);
    _pool->release(_buffer);
}
