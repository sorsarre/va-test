#include <map>
#include <mutex>
#include <condition_variable>
#include <limits>

#include "hash_collector_impl.h"

using namespace hasher;

//--------------------------------------------------------------------------
struct hash_collector_impl::impl
{
    std::mutex mutex;
    std::condition_variable cond;
    std::map<uint64_t, std::string> results;
    uint64_t target_count;
    file_handle::ptr output;
};

//--------------------------------------------------------------------------
void hash_collector_impl::collect(uint64_t index, const std::string& hash) {
    std::lock_guard<std::mutex> lock(_impl->mutex);
    _impl->results[index] = hash;
    if (_impl->results.size() == _impl->target_count) {
        _impl->cond.notify_all();
    }
}

//--------------------------------------------------------------------------
void hash_collector_impl::flush(uint64_t count) {
    std::unique_lock<std::mutex> lock(_impl->mutex);

    _impl->target_count = count;

    while (_impl->results.size() < count) {
        _impl->cond.wait(lock);
    }

    for (const auto& pair: _impl->results) {
        fprintf(_impl->output->get(), "%s\n", pair.second.c_str());
    }

    _impl->results.clear();
    fflush(_impl->output->get());

    _impl->target_count = std::numeric_limits<uint64_t>::max();
}

//--------------------------------------------------------------------------
void hash_collector_impl::done() {
    fflush(_impl->output->get());
}

//--------------------------------------------------------------------------
hash_collector_impl::hash_collector_impl(file_handle::ptr output) {
    _impl.reset(new impl);
    _impl->output = output;
}