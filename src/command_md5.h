#pragma once

#include "command.h"
#include "shared_buffer.h"
#include "buffer_pool.h"
#include "hash_collector.h"

namespace hasher
{
    //----------------------------------------------------------------------
    class command_md5: public command
    {
    public:
        command_md5(
                uint64_t index,
                shared_buffer buffer,
                std::shared_ptr<hash_collector> collector,
                std::shared_ptr<buffer_pool> pool);

        virtual void exec() const override;

    private:
        uint64_t _index;
        shared_buffer _buffer;
        std::shared_ptr<hash_collector> _collector;
        std::shared_ptr<buffer_pool> _pool;
    };
}