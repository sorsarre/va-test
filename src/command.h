#pragma once

namespace hasher
{
    //--------------------------------------------------------------------------
    class command
    {
    public:
        virtual void exec() const = 0;
        virtual ~command() = default;
    };
}