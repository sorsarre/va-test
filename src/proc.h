#pragma once

#include <memory>
#include "command.h"

namespace hasher
{
    class proc
    {
    public:
        virtual void stop() = 0;
        virtual void push(std::shared_ptr<command> cmd) = 0;

        static std::shared_ptr<proc> create(uint32_t threads = 0);

        virtual ~proc() = default;
    };
}