#pragma once

#include "shared_buffer.h"

namespace hasher
{
    //--------------------------------------------------------------------------
    class buffer_pool
    {
    public:
        virtual shared_buffer acquire() = 0;
        virtual void release(const shared_buffer& buf) = 0;

        virtual ~buffer_pool() = default;
    };
}