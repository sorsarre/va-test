#include "block_reader.h"

using namespace hasher;

//--------------------------------------------------------------------------
struct block_reader::impl
{
    std::shared_ptr<file_handle> input;
};

//--------------------------------------------------------------------------
block_reader::block_reader()
{
    _impl.reset(new impl);
}

//--------------------------------------------------------------------------
block_reader::ptr block_reader::open(std::shared_ptr<file_handle> fh) {
    ptr ret(new block_reader());
    ret->_impl->input = fh;
    return ret;
}

//--------------------------------------------------------------------------
size_t block_reader::read(shared_buffer& buf) {
    auto read = fread(buf.get(), 1, buf.capacity(), _impl->input->get());
    buf.size(read);
    return read;
}

//--------------------------------------------------------------------------
bool block_reader::done() {
    return 0 != feof(_impl->input->get());
}

//--------------------------------------------------------------------------
block_reader::~block_reader() {
}
