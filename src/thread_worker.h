#pragma once

#include "command.h"
#include <memory>
#include <thread>
#include <mutex>
#include <condition_variable>
#include <boost/thread/sync_queue.hpp>
#include <iostream>

namespace hasher
{
    class thread_worker
    {
    public:
        typedef std::shared_ptr<command> queue_entry_type;
        typedef boost::sync_queue<queue_entry_type> queue_type;
        typedef std::shared_ptr<queue_type> queue_ptr_type;

        thread_worker(queue_ptr_type queue)
                : _stopping(false)
                , _queue(queue)
                , _thread(&thread_worker::work, std::ref(*this))
        {

        }

        void stop() {
            _stopping = true;
        }

        void join() {
            if (_thread.joinable()) {
                _thread.join();
            }
        }
    private:
        static void work(thread_worker& w)
        {
            while (true) {
                if (w._stopping) {
                    break;
                }

                try {
                    auto cmd = w._queue->pull();
                    cmd->exec();
                } catch(const std::exception& e) {
                    std::cerr << "Error in worker: " << e.what() << std::endl;
                }
            }
        }


        bool _stopping;
        queue_ptr_type _queue;
        std::thread _thread;
    };
}