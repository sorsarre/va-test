#include "proc.h"
#include "proc_sync.h"
#include "proc_threads.h"

using namespace hasher;

std::shared_ptr<proc> proc::create(uint32_t num) {
    auto threads = num ? num : std::thread::hardware_concurrency();
    switch(threads) {
        case 0:
            return std::make_shared<proc_threaded>(2);
        case 1:
            return std::make_shared<proc_sync>();
        default:
            return std::make_shared<proc_threaded>(threads);
    }
}