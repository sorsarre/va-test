#pragma once

#include "proc.h"
#include "thread_worker.h"
#include "command_null.h"
#include <boost/thread/sync_queue.hpp>
#include <thread>
#include <vector>

namespace hasher
{
    //--------------------------------------------------------------------------
    class proc_threaded: public proc
    {
    public:
        proc_threaded(size_t workers);

        virtual void stop() override;
        virtual void push(std::shared_ptr<command> cmd) override;

    private:
        thread_worker::queue_ptr_type _input;
        std::vector<thread_worker> _workers;
    };

    //--------------------------------------------------------------------------
    proc_threaded::proc_threaded(size_t workers) {
        _input = std::make_shared<thread_worker::queue_type>();
        _workers.reserve(workers);
        for (size_t iter = 0; iter < workers; ++iter) {
            _workers.emplace_back(_input);
        }
    }

    //--------------------------------------------------------------------------
    void proc_threaded::stop() {
        // 1. stop all the workers
        // 2. send as many "null" commands as there are workers
        // 3. wait for all workers to stop

        for (auto& w: _workers) {
            w.stop();
        }

        for (auto& w: _workers) {
            _input->push(std::make_shared<command_null>());
        }

        for (auto& w: _workers) {
            w.join();
        }
    }

    //--------------------------------------------------------------------------
    void proc_threaded::push(std::shared_ptr<command> cmd) {
        _input->push(cmd);
    }
}