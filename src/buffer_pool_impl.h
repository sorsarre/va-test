#pragma once

#include "buffer_pool.h"
#include <unordered_set>
#include <mutex>
#include <condition_variable>

namespace hasher
{
    //--------------------------------------------------------------------------
    class buffer_pool_impl: public buffer_pool
    {
    public:
        //----------------------------------------------------------------------
        buffer_pool_impl(size_t size, size_t count) {
            for (size_t iter = 0; iter < count; ++iter) {
                _idle.emplace(size);
            }
        }

        //----------------------------------------------------------------------
        shared_buffer acquire() override {
            std::unique_lock<std::mutex> lock(_mutex);
            while (_idle.empty()) {
                _cond.wait(lock);
            }

            auto buffer = *_idle.begin();
            _idle.erase(buffer);
            _active.insert(buffer);

            return buffer;
        }

        //----------------------------------------------------------------------
        void release(const shared_buffer& buf) override {
            std::lock_guard<std::mutex> lock(_mutex);
            const_cast<shared_buffer&>(buf).size(0);
            _active.erase(buf);
            _idle.insert(buf);
            _cond.notify_one();
        }
    private:
        //----------------------------------------------------------------------
        struct shared_buffer_hash {
            intptr_t operator()(const shared_buffer& buf) const {
                return reinterpret_cast<intptr_t>(buf.get());
            }
        };

        //----------------------------------------------------------------------
        typedef std::unordered_set<shared_buffer, shared_buffer_hash> buffer_set;

        buffer_set _active;
        buffer_set _idle;

        std::mutex _mutex;
        std::condition_variable _cond;
    };
}