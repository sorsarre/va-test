#pragma once

#include <memory>
#include <cassert>
#include <cstdint>
#include <exception>
#include <numeric>

namespace hasher
{
    //--------------------------------------------------------------------------
    class shared_buffer
    {
    public:
        //----------------------------------------------------------------------
        shared_buffer(size_t size) {
            if (!size) {
                throw std::logic_error("Cannot allocate buffer of size zero");
            }

            _data = std::make_shared<buffer_data>(size);
        }

        //----------------------------------------------------------------------
        shared_buffer(const shared_buffer& buf): _data(buf._data) {

        }

        //----------------------------------------------------------------------
        shared_buffer clone() const {
            auto new_data = std::make_shared<buffer_data>(_data->capacity);
            new_data->size = _data->size;
            std::copy(_data->data, _data->data + _data->size, new_data->data);
            return shared_buffer(new_data);
        }

        //----------------------------------------------------------------------
        uint8_t* get() {
            return _data->data;
        }

        //----------------------------------------------------------------------
        const uint8_t* get() const {
            return _data->data;
        }

        //----------------------------------------------------------------------
        size_t size() const {
            return _data->size;
        }

        //----------------------------------------------------------------------
        void size(size_t sz) {
            _data->size = std::min(sz, _data->capacity);
        }

        //----------------------------------------------------------------------
        size_t capacity() const {
            return _data->capacity;
        }

        //----------------------------------------------------------------------
        bool operator==(const shared_buffer& other) const {
            return _data == other._data;
        }

    private:
        //----------------------------------------------------------------------
        struct buffer_data {
            size_t size;
            size_t capacity;
            uint8_t* data;

            buffer_data(size_t _size): size(0), capacity(_size), data(nullptr) {
                data = new uint8_t[_size];
            }

            ~buffer_data() {
                delete[] data;
            }
        };

        //----------------------------------------------------------------------
        shared_buffer(std::shared_ptr<buffer_data> data): _data(data) {
        }

        std::shared_ptr<buffer_data> _data;
    };
}