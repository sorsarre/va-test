#include "proc_sync.h"

//--------------------------------------------------------------------------
void hasher::proc_sync::stop() {

}

//--------------------------------------------------------------------------
void hasher::proc_sync::push(std::shared_ptr<hasher::command> cmd) {
    if (cmd) {
        cmd->exec();
    }
}
