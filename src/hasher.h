#pragma once

#include "buffer_pool.h"
#include "proc.h"
#include "hash_collector.h"

namespace hasher
{
    //--------------------------------------------------------------------------
    class hasher
    {
    public:
        //----------------------------------------------------------------------
        hasher(
                std::shared_ptr<buffer_pool> pool,
                std::shared_ptr<proc> proc,
                std::shared_ptr<hash_collector> collector,
                uint64_t threshold);

        void push(const shared_buffer& buf);
        void done();
    private:
        uint64_t _index;
        uint64_t _threshold;
        std::shared_ptr<hash_collector> _collector;
        std::shared_ptr<buffer_pool> _pool;
        std::shared_ptr<proc> _proc;
    };
}