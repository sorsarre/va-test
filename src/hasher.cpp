#include "hasher.h"
#include "command_md5.h"

//----------------------------------------------------------------------
hasher::hasher::hasher(
        std::shared_ptr<buffer_pool> pool,
        std::shared_ptr<proc> proc,
        std::shared_ptr<hash_collector> collector,
        uint64_t threshold)
        : _index(0)
        , _threshold(threshold)
        , _collector(collector)
        , _pool(pool)
        , _proc(proc)
{

}

//----------------------------------------------------------------------
void hasher::hasher::push(const shared_buffer& buf) {
    if (_index >= _threshold) {
        _collector->flush(_index);
        _index = 0;
    }

    ++_index;
    auto cmd = std::make_shared<command_md5>(_index, buf, _collector, _pool);
    _proc->push(cmd);
}

//----------------------------------------------------------------------
void hasher::hasher::done() {
    if (_index > 0) {
        _collector->flush(_index);
    }

    _proc->stop();
    _collector->done();
}