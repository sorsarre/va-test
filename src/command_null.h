#pragma once

#include "command.h"

namespace hasher
{
    //----------------------------------------------------------------------
    class command_null: public command
    {
    public:
        virtual void exec() const override;
    };
}