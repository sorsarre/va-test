#pragma once

#include <memory>
#include <cstdio>
#include "hash_collector.h"
#include "file_handle.h"

namespace hasher
{
    class hash_collector_impl: public hash_collector
    {
    public:
        hash_collector_impl(file_handle::ptr file);
        virtual void collect(uint64_t index, const std::string& hash) override;
        virtual void flush(uint64_t count) override;
        virtual void done() override;

    private:
        struct impl;
        std::shared_ptr<impl> _impl;
    };
}