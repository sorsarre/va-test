#pragma once
#include <cstdio>
#include <memory>
#include "shared_buffer.h"
#include "file_handle.h"

namespace hasher
{
    class block_reader
    {
    public:
        typedef std::unique_ptr<block_reader> ptr;

        static ptr open(std::shared_ptr<file_handle> handle);

        size_t read(shared_buffer& buf);
        bool done();
        block_reader();
        ~block_reader();
    private:
        struct impl;
        std::unique_ptr<impl> _impl;
    };
}