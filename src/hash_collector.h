#pragma once
#include <cstdint>
#include <string>

namespace hasher
{
    //--------------------------------------------------------------------------
    class hash_collector
    {
    public:
        virtual void collect(uint64_t index, const std::string& hash) = 0;
        virtual void flush(uint64_t count) = 0;
        virtual void done() = 0;

        virtual ~hash_collector() = default;
    };
}