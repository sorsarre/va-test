### Overview

This software generates a block-wise MD5 signature for a specified file,
i.e. it computes an MD5 hash for each block of specified size in the file.

### Usage

```
va_test [OPTIONS] [INPUT] [OUTPUT] [BLOCKSIZE]
Allowed options:
  -h [ --help ]                 produce help message
  -i [ --input ] arg (=-)       input file (- for stdin, default)
  -o [ --output ] arg (=-)      output file(- for stdout, default)
  -b [ --block ] arg (=1048576) block size
  -t [ --threads ] arg (=0)     number of threads to use (0 - auto)
  -f [ --flush ] arg (=128)     number of blocks to process before flushing the
                                results
```

### Building from source

#### Unix

```
cd <build_dir>
cmake -DCMAKE_BUILD_TYPE=Release -DBOOST_ROOT=<path-to-boost> <src_dir>

# if boost is installed system-wide, -DBOOST_ROOT=... can be omitted

make -j4
```

#### Windows

```
cd <build_dir>
cmake -G "Visual Studio 14 2015" -DBOOST_ROOT=<path-to-boost>
```

After that, the generated solution file can be opened in MSVC as usual.